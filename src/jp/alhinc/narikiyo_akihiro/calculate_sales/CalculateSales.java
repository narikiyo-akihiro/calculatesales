package jp.alhinc.narikiyo_akihiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {

		HashMap<String, String> map = new HashMap<>(); //支店番号と支店名を格納する
		HashMap<String, Long> map2 = new HashMap<>(); //支店番号と売上金額を格納する

		//支店定義ファイルを検索し表示させる
		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");

			if (!(file.exists())) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String[] lines = null;

			String line;
			while ((line = br.readLine()) != null) {

				//，で区切る
				lines = line.split(",", -1);

				if (lines.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				map.put(lines[0], lines[1]);
				map2.put(lines[0], 0L);

				if (!(lines[0].matches("^[0-9]{3}") && lines[1].matches(".*$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました1");
			return;

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		List<String> branchCode = new ArrayList<String>(map.keySet());

		try {
			FilenameFilter filter = new FilenameFilter() { //検索したいフィルタを作成する
				public boolean accept(File file, String str) {

					if (str.matches("^[0-9]{8}\\.rcd$")) { //0~9の数字8桁から始まり、[.rcd]で終わるファイル
						return true;
					} else {
						return false;
					}
				}
			};

			//listFilesメソッドを使用して一覧を取得する
			File[] list = new File(args[0]).listFiles(filter);

			//売上ファイルが連番になっていない場合
			List<Integer> number = new ArrayList<Integer>();

			for (int i = 0; i < list.length; i++) {
				String fileName = list[i].getName();
				number.add(Integer.parseInt(fileName.substring(0, 8)));
			}
			Collections.sort(number);

			for (int i = 1; i < number.size(); i++) {
				if (number.get(i) != number.get(i - 1) + 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			for (int i = 0; i < list.length; i++) {
				try {
					FileReader fr = new FileReader(list[i]);
					br = new BufferedReader(fr);

					String line;
					line = br.readLine();
					String line2;
					line2 = br.readLine();

					//支店に該当がなかった場合
					if (!(branchCode.contains(line))) {
						System.out.println(list[i].getName() + "の支店コードが不正です");
						return;
					}
					//ファイルの中身が3行以上ある場合
					String line3;
					line3 = br.readLine();
					if (line3 != null) {
						System.out.println(list[i].getName() + "のフォーマットが不正です");
						return;
					}

					long a = map2.get(line); //集計元の数
					long b = Long.parseLong(line2);//売上ファイルの金額
					long c = a + b;

					map2.put(line, c);

					//合計金額が10桁を超えた場合のエラー処理
					if (c > 9999999999L) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}
			}
			try {
				//出力先を作成
				FileWriter fw = new FileWriter(new File(args[0], "branch.out"));
				PrintWriter pw = new PrintWriter(new BufferedWriter(fw));

				//内容を指定する(支店コード、支店名、合計金額を順番に表示したい)
				for (Entry<String, String> entry : map.entrySet()) {
					pw.println(entry.getKey() + ":" + entry.getValue() + ":" + map2.get(entry.getKey()));
				}

				//ファイルに書き出す
				pw.close();

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			}
		}
	}
}
